'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { Button } from '@nextui-org/button';
import { Input } from '@nextui-org/input';
import { Controller, useForm } from 'react-hook-form';

import { formLoginSchema, FormLoginSchemaType } from '@/lib/schema/login';

export function FormLogin() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormLoginSchemaType>({
    mode: 'onChange',
    resolver: zodResolver(formLoginSchema),
  });

  /** Implement OnSubmit Function */
  const onSubmit = async (values: FormLoginSchemaType) => {
    console.log(values);
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="relative mt-6 flex h-full flex-col gap-8"
    >
      <Controller
        control={control}
        name="username"
        render={({ field }) => (
          <Input
            {...field}
            label="Username"
            labelPlacement="outside"
            placeholder="Enter your username"
            radius="sm"
            variant="bordered"
            errorMessage={errors.username?.message}
            isRequired
          />
        )}
      />
      <Controller
        control={control}
        name="password"
        render={({ field }) => (
          <Input
            {...field}
            isRequired
            type="password"
            label="Password"
            labelPlacement="outside"
            placeholder="Enter your password"
            radius="sm"
            variant="bordered"
            errorMessage={errors.password?.message}
          />
        )}
      />
      <Button color="primary" radius="sm" type="submit" className="mt-6">
        Login
      </Button>
    </form>
  );
}
