import { AppLayout } from '@/app/_components/layout';

export default function MainLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <AppLayout>
      <div className="mt-6 h-full !border-small bg-background">{children}</div>
    </AppLayout>
  );
}
