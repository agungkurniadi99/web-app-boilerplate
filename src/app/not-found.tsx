import Link from 'next/link';
import { Button } from '@nextui-org/button';

// million-ignore
export default function NotFound() {
  return (
    <div className="grid h-screen place-content-center space-y-4 bg-white px-4 text-base">
      <header className="text-center">
        <h1 className="text-9xl font-black text-zinc-200">404</h1>
        <p className="text-2xl font-bold tracking-tight text-zinc-900 sm:text-4xl">
          Uh-oh!
        </p>
      </header>
      <p className="text-center text-zinc-500">We can&apos;t find that page.</p>
      <Link href={'/'} passHref legacyBehavior>
        <Button color="primary" radius="sm">
          Go To Dashboard
        </Button>
      </Link>
    </div>
  );
}
