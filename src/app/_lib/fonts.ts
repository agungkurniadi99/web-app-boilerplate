import { DM_Sans } from 'next/font/google';
import localFont from 'next/font/local';

export const DMSans = DM_Sans({
  display: 'swap',
  weight: 'variable',
  subsets: ['latin'],
  variable: '--font-dm-sans',
});

export const GeneralSans = localFont({
  src: '../_assets/fonts/GeneralSans.ttf',
  display: 'swap',
  variable: '--font-general-sans',
});
