import { create } from 'zustand';

interface SidenavStoreType {
  isOpen: boolean;
  toggle: () => void;
}

export const useSidenavStore = create<SidenavStoreType>()((set) => ({
  isOpen: false,
  toggle: () => set((state) => ({ isOpen: !state.isOpen })),
}));
