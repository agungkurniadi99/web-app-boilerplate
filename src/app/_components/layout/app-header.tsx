'use client';

import {
  Avatar,
  Button,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownTrigger,
} from '@nextui-org/react';
import { useAtom } from 'jotai';
import { LogOut, PanelLeftClose, PanelLeftOpen, User2 } from 'lucide-react';

import { sidebarAtom } from '@/app/_lib/atoms';

export function AppHeader() {
  const [open, setOpenSidebar] = useAtom(sidebarAtom);

  return (
    <header className="relative flex h-16 items-center gap-unit-sm rounded-medium !border-small bg-background px-6">
      <Button
        isIconOnly
        variant="light"
        size="sm"
        aria-label="panel-close"
        onClick={() => setOpenSidebar((state) => !state)}
      >
        {open ? (
          <PanelLeftOpen strokeWidth={2.5} size={20} />
        ) : (
          <PanelLeftClose strokeWidth={2.5} size={20} />
        )}
      </Button>
      <div className="flex-1">
        <h2 className="tracking w-fit text-large font-semibold">Dashboard</h2>
      </div>
      <Dropdown placement="bottom-end">
        <DropdownTrigger>
          <Avatar
            as="button"
            src={'https://i.pravatar.cc/150?u=a04258114e29026702d'}
            icon={<User2 size={20} strokeWidth={2.5} />}
            className="transition-transform"
          />
        </DropdownTrigger>
        <DropdownMenu aria-label="Username Action">
          <DropdownItem key="user-info" isReadOnly showDivider>
            <p className="font-semibold leading-none">Agung Kurniadi</p>
            <p>Super Admin</p>
          </DropdownItem>
          <DropdownItem
            key="logout"
            color="danger"
            startContent={<LogOut size={14} strokeWidth={2} />}
            onClick={() => console.log('Logout Action')}
          >
            <p>Logout</p>
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </header>
  );
}
