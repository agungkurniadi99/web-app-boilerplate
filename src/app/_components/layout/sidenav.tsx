'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { ScrollShadow } from '@nextui-org/react';
import { useAtomValue } from 'jotai';
import { BarChart2, NotebookPen, User2 } from 'lucide-react';

import { sidebarAtom } from '@/app/_lib/atoms';
import { cn } from '@/app/_lib/utils';

export function SideNavigation() {
  const pathname = usePathname();
  const open = useAtomValue(sidebarAtom);

  const navigationRoute = [
    {
      label: 'Dashboard',
      key: 'dashboard',
      children: [
        {
          href: `/`,
          label: 'Overview',
          active: pathname === '/',
          icon: <BarChart2 strokeWidth={2.5} size={18} />,
        },
      ],
    },
    {
      label: 'Masters',
      key: 'masters',
      children: [
        {
          href: `/users`,
          label: 'Users',
          active: pathname === '/users',
          icon: <User2 strokeWidth={2.5} size={18} />,
        },
      ],
    },
    {
      label: 'Reports',
      key: 'reports',
      children: [
        {
          href: `/activity`,
          label: 'Activity',
          active: pathname === '/activity',
          icon: <NotebookPen strokeWidth={2.5} size={18} />,
        },
      ],
    },
  ];

  return (
    <ScrollShadow hideScrollBar className="h-full pt-4">
      <nav className="flex flex-auto flex-col gap-y-0.5 space-y-2">
        {navigationRoute.map((nav) => (
          <SideNavItem
            key={nav.key}
            label={nav.label}
            items={nav.children}
            isOpen={open}
          />
        ))}
      </nav>
    </ScrollShadow>
  );
}

function SideNavItem({
  label,
  items,
  isOpen,
}: {
  label: string;
  items: { href: string; label: string; active: boolean; icon: JSX.Element }[];
  isOpen: boolean;
}) {
  return (
    <div>
      {!isOpen ? (
        <span className="inline-block text-small font-semibold">{label}</span>
      ) : null}
      <ul className="mt-2 space-y-1">
        {items.map((r) => (
          <li key={r.href}>
            <Link
              href={r.href}
              className={cn(
                'flex items-center rounded-medium p-2 text-foreground-600 transition-colors hover:bg-foreground-200 hover:text-foreground-900',
                r.active ? 'bg-foreground-100 text-foreground-900' : ''
              )}
            >
              <span className={cn('mr-2 block', isOpen && 'm-0')}>
                {r.icon && r.icon}
              </span>
              <p className="flex-1">{!isOpen && r.label}</p>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
