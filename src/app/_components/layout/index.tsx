'use client';

import Image from 'next/image';
import Link from 'next/link';
import { useAtomValue } from 'jotai';

import CrownLogo from '@/app/_assets/logo/crown-light.svg';
import CrownIcon from '@/app/_assets/logo/crown.svg';
import { sidebarAtom } from '@/app/_lib/atoms';
import { cn } from '@/app/_lib/utils';

import { AppHeader } from './app-header';
import { SideNavigation } from './sidenav';

export function AppLayout({ children }: { children: React.ReactNode }) {
  const open = useAtomValue(sidebarAtom);

  return (
    <section className="relative flex h-full">
      <aside
        className={cn(
          'relative flex h-screen w-80 min-w-16 flex-col !border-r-small border-r-divider bg-white p-6 transition-width',
          open && 'w-[86px]'
        )}
      >
        <div className="flex h-16 items-center !border-b-small border-b-divider">
          <Link href="/" passHref>
            <span className="sr-only">Home</span>
            <Image
              src={!open ? CrownLogo : CrownIcon}
              alt="Crown"
              className="mb-6 flex h-full items-center"
            />
          </Link>
        </div>
        <SideNavigation />
      </aside>
      <main className="flex w-full flex-auto flex-col p-6">
        <AppHeader />
        {children}
      </main>
    </section>
  );
}
