import { nextui } from '@nextui-org/react';

const config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        geist: ['var(--font-geist)'],
        ['dm-sans']: ['var(--font-dm-sans)'],
        ['general-sans']: ['var(--font-general-sans)'],
      },
    },
  },
  darkMode: 'class',
  plugins: [
    nextui({
      layout: {
        disabledOpacity: 0.4,
        radius: {
          small: '2px',
          medium: '4px',
          large: '8px',
        },
        borderWidth: {
          small: '1px',
          medium: '2px',
          large: '4px',
        },
        fontSize: {
          large: '20px',
          medium: '14px',
          small: '12px',
        },
      },
    }),
  ],
};

export default config;
